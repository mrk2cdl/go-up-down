package main

import (
	"archive/tar"
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"text/template"
	"time"

	"github.com/joho/godotenv"
)

type TokenResponse struct {
	Token string
}

type ManifestResponse struct {
	FsLayers []Layer
}

type Layer struct {
	BlobSum string
}

type TagsList struct {
	Tags []string
}

func getToken(client *http.Client, scope string) string {
	var tokenResponse TokenResponse
	tokenReq, _ := http.NewRequest("GET", os.Getenv("AUTH_URL")+os.Getenv("REPOSITORY")+":"+scope, nil)
	if scope == "push%2Cpull" {
		tokenReq.Header.Add("Authorization", "Basic "+os.Getenv("TOKEN_AUTH"))
	}
	tokenResp, _ := client.Do(tokenReq)
	tokenBody, _ := io.ReadAll(tokenResp.Body)
	json.Unmarshal(tokenBody, &tokenResponse)
	return tokenResponse.Token
}

func makeTar(file multipart.File, handler multipart.FileHeader) string {
	out, _ := os.CreateTemp("", "goud-temp-")
	tw := tar.NewWriter(out)
	header := new(tar.Header)
	header.Name = handler.Filename
	header.Size = handler.Size
	header.Mode = int64(0777)
	tw.WriteHeader(header)
	io.Copy(tw, file)
	tw.Close()
	out.Close()
	return out.Name()
}

func makeRequest(client *http.Client, token string, method string, url string, data io.Reader, manifest bool) *http.Response {
	req, err := http.NewRequest(method, url, data)
	if err != nil {
		fmt.Println(err.Error())
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %v", token))
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err.Error())
	}
	return resp
}

func uploadData(client *http.Client, token string, data io.Reader, start time.Time) (string, string, time.Time, int) {
	hashWriter := sha256.New()
	tr := io.TeeReader(data, hashWriter)
	location := makeRequest(client, token, "POST", os.Getenv("API_URL")+os.Getenv("REPOSITORY")+"/blobs/uploads/", nil, false)
	upload := makeRequest(client, token, "PATCH", location.Header.Get("location"), tr, false)
	hash := fmt.Sprintf("%x", hashWriter.Sum(nil))
	if time.Since(start).Seconds() >= 300 {
		token = getToken(client, "push%2Cpull")
		start = time.Now()
	}
	digest := makeRequest(client, token, "PUT", upload.Header.Get("location")+"&digest=sha256%3A"+hash, nil, false)
	blob := makeRequest(client, token, "HEAD", digest.Header.Get("location"), nil, false)

	return hash, token, start, blob.StatusCode
}

func main() {
	godotenv.Load()
	client := &http.Client{}

	http.HandleFunc("/download", func(w http.ResponseWriter, r *http.Request) {
		tag := r.URL.Query().Get("file")

		token := getToken(client, "pull")

		var manifestResponse ManifestResponse
		manifestResp := makeRequest(client, token, "GET", os.Getenv("API_URL")+os.Getenv("REPOSITORY")+"/manifests/"+tag, nil, false)
		manifestBody, _ := io.ReadAll(manifestResp.Body)
		json.Unmarshal(manifestBody, &manifestResponse)

		s := manifestResponse.FsLayers[0].BlobSum

		fileResp := makeRequest(client, token, "GET", os.Getenv("API_URL")+os.Getenv("REPOSITORY")+"/blobs/"+s, nil, false)

		fileTar := tar.NewReader(fileResp.Body)
		h, _ := fileTar.Next()
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", h.Name))
		w.Header().Set("Content-Length", fmt.Sprint(h.Size))
		io.Copy(w, fileTar)

	})
	tmpl, _ := template.ParseFiles("hello.html")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		token := getToken(client, "pull")
		var tagsList TagsList
		tagsResp := makeRequest(client, token, "GET", os.Getenv("API_URL")+os.Getenv("REPOSITORY")+"/tags/list", nil, false)
		tagsBody, _ := io.ReadAll(tagsResp.Body)
		json.Unmarshal(tagsBody, &tagsList)

		tmpl.Execute(w, tagsList)
	})
	http.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {
		file, handler, _ := r.FormFile("file")

		fName := makeTar(file, *handler)
		file.Close()

		temp, err := os.Open(fName)
		if err != nil {
			fmt.Println(err.Error())
		}
		bufSize, err := temp.Stat()
		if err != nil {
			fmt.Println(err.Error())
		}
		start := time.Now()
		token := getToken(client, "push%2Cpull")
		tarDigest, token, start, _ := uploadData(client, token, temp, start)
		temp.Close()
		os.Remove(fName)
		configJson := []byte(`{"sha256":"` + tarDigest + `"}`)

		configDigest, token, _, _ := uploadData(client, token, bytes.NewBuffer(configJson), start)
		manifestJson := []byte(`{"config":{"sha256":"` + configDigest + `"},"tar":{"sha256":"` + tarDigest + `"}}`)
		makeRequest(client, token, "PUT", os.Getenv("API_URL")+os.Getenv("REPOSITORY")+"/manifests/"+string(handler.Filename), bytes.NewBuffer(manifestJson), true)

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
	http.ListenAndServe("127.0.0.1:5000", nil)
}

# Go Up Down

A simple web app to forward downloads from an API to the end user and/or forward uploaded files from the end user to an API.

## Endpoints

`GET /`

Simple UI that shows a list of downloadable items and an upload box for adding files to the list.

`GET /download?file=<file>`

Serves a file.

`POST /upload`

Uploads a file received via `multipart/form-data`